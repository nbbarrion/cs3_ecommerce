import './App.css';

import Home from './pages/Home';
import Footer from './pages/Footer';
import Products from './pages/Catalog';
import Login from './pages/Login';
import Signup from './pages/Register';
import Error from './pages/Error';
import AppNavbar from './components/Navbar';

import { Route, Switch, BrowserRouter as Router} from 'react-router-dom';


  
function App() {
  return (
    <Router>
      <AppNavbar />

      <Switch>
        <Route exact path="/" component = {Home} />
        <Route exact path="/products" component = {Products} />
        <Route exact path="/login" component = {Login}/>
        <Route exact path="/register" component = {Signup} />
        <Route component = {Error} />
        <Footer/>
      </Switch>

    </Router>
  );
}

export default App;
