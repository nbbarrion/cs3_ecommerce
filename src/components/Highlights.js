import { Jumbotron, Container} from 'react-bootstrap';
import Logo from "../assets/logo.png";


export default function Highlights () {
    return(
        <Jumbotron className="text-center">
            <Container>
                {/* <video className="video-info container-center-fluid" src='../public/video-2.mp4' autoPlay loop muted/> */}
                <h1>Welcome!</h1>
                <p>
                This is an e-Commerce website that will help you to find the best items you want most at exactly very affordable price.
                </p>
                <img src={Logo}
                width="200px"
                height="200px"
                alt="Image Unavailable" 
                fluid />
            </Container>
        </Jumbotron>
    );
}