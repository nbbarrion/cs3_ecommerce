import { Navbar, Nav, Badge} from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import Logo from "../assets/logo.png";
import "./Navbar.css"

export default function AppNavbar() {
  // const [openLinks, setOpenLinks] = useState(false);

  // const toggleNavbar = () => {
  //   setOpenLinks(!openLinks);
  // };
    return (
      <Navbar bg="dark" expand="xl">
        <img src={Logo}
        width="120"
        height="60"
        className="d-inline-block align-top"
        alt="Image Unavailable" 
        />
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse className="justify-content-end" id="basic-navbar-nav">
                <Nav.Link className="text-success" as={NavLink} to="/" >Home</Nav.Link>
                <Nav.Link className="text-success" as={NavLink} to="/register" >Register</Nav.Link>
                <Nav.Link className="text-success" as={NavLink} to="/products" >Products</Nav.Link>
                <Nav.Link className="text-success" as={NavLink} to="/login" >Login</Nav.Link>
            </Navbar.Collapse>
      </Navbar>
    )
};