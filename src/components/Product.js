
import {Card, Row, Col, Container, CardColumns} from 'react-bootstrap'
export default function CourseCard({productInfo}) {

const { name, description, price } = productInfo;
    return(
        <Container>
            <CardColumns>
            <Row className="m-3">
                <Col col-xs={12} col-md={6}>
                    <Card className="cardHighlight text-center ds-flex-start">
                        <Card.Body>
                            <Card.Title> {name} </Card.Title>
                            <Card.Subtitle> Product Description: </Card.Subtitle>
                            <Card.Text>
                                {description}
                            </Card.Text>
                            <Card.Subtitle> Product Price: </Card.Subtitle>
                            <Card.Text> &#8369; {price} </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            </CardColumns>
        </Container>
    )
}