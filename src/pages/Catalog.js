import {useEffect, useState} from 'react';
import Hero from '../components/Banner';
import SubjectCard from '../components/Product';
import Footer from '../pages/Footer';

let info = {
    title: 'Product Page'
}


export default function Products() {

   
    const [ products, setProducts ] = useState([]);
    useEffect(() => {
        fetch('https://hidden-woodland-25067.herokuapp.com/courses/active').then(res => res.json()).then(convertedData => {
            setProducts(convertedData.map(subject => {
                return(
                   
                    <SubjectCard key={subject._id} productInfo={subject} />
                )
            }))
        });
    })
    return(
        <>
            <Hero data={info}/>
            {products}
            <Footer foot="fixed-bottom"/>
        </>
    );
}