import Hero from "../components/Banner";
import { Container } from 'react-bootstrap';
import Footer from '../pages/Footer';

let Invalid = {
    title: "404 Page Not Found",
    tagline: "You are trying to access a Non Existing Page"
}

export default function Error() {
    return(
        <Container>
            <Hero data={Invalid}/>
            <Footer foot="fixed-bottom"/>
        </Container>
    );
}