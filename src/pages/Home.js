//Identify the components needed in order to properly build your page.
import Hero from '../components/Banner';
import Showcase from '../components/Highlights';
import Storage from 'react-bootstrap/Container';
import Footer from '../pages/Footer';

let details = {
    title: "UNik's E-Commerce app"
}

export default function Home () {
    return(
        <>
        <Storage>
        <Hero data={details} />
        <Showcase></Showcase>
        </Storage>
        <Footer foot="fixed-bottom"/>
        </>
    );
}