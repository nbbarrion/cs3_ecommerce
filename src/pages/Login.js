import { Form, Button, Container } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Hero from '../components/Banner';
import Footer from '../pages/Footer';


const bannerLabel = {
    title: 'User Login',
    tagline: 'Access your account and start check our best products here!'
}

export default function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

        const authenticate = (e) => {
            e.preventDefault()

            fetch('https://stark-ravine-07314.herokuapp.com/users/login',{
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                // console.log(data)
                //localStorage - stores/contains information to our web pages (keeps the data forever)
                //setItem -set data to our localStorage
                //getItem -get data to our localStorage
                localStorage.setItem('accessToken', data.accessToken);
            })
        }
        useEffect(()=>{
            if (email !== "" && password !== "") {
                setIsActive(true)
            } else {
                setIsActive(false)
            }
        }, [email, password]);
    return(
        <card>
            <Container>
                {/* Banner of the page */}
                <Hero data={bannerLabel}/>

                <Form className="mb-5">
                    <Form.Group controlId="userEmail">
                        <Form.Label> Email Address: </Form.Label>
                        <Form.Control 
                        type="email" 
                        placeholder="Insert your Email Here"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required/>
                    </Form.Group>
                    <Form.Group controlId="userPassword">
                        <Form.Label> Password: </Form.Label>
                        <Form.Control 
                        type="password" 
                        placeholder="Insert your Password Here"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        required/>
                    </Form.Group>
                    {
                        isActive ?
                        <Button type="submit" id="submitBtn" className="btn btn-block"> Click Here </Button>
                        :
                        <Button type="submit" id="submitBtn" className="btn btn-block" disabled> Click Here </Button>
                    }
                </Form>
            </Container>
            <Footer foot="fixed-bottom"/>
        </card>
    );
};