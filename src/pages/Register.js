
import { useState, useEffect } from 'react';
import { Form, Button, Container } from "react-bootstrap";
import Swal from 'sweetalert2';
import { Redirect, useHistory } from 'react-router-dom';
import Hero from "../components/Banner";
import Footer from '../pages/Footer';


const forBanner = {
    title: "Create your Account Here!",
    tagline: "Please give your details below:"
}


export default function Register() {
   
    const history = useHistory();
    const [ firstName, setFirstName ]= useState('');
    const [ middleName, setMiddleName ] = useState('');
    const [ lastName, setLastName ] = useState('');
    const [ email, setEmail ] = useState('');
    const [ password1, setPassword1 ] = useState('');
    const [gender, setGender] = useState('Male');
    const [ password2, setPassword2 ] =useState('');
    const [ mobileNo, setMobileNo ] = useState(''); 
    const [ isRegisterBtnActive, setRegisterBtnActive ] = useState('');
    const [ isComplete, setIsComplete ] = useState(false);
    const [ isMatch, setIsMatch ] = useState(false);
    const [ isValidNumber, setIsValidNumber ] = useState(false);

    function registerUser(event) {
        event.preventDefault();

        // fetch('https://hidden-woodland-25067.herokuapp.com/users/register')
        //describe a procedure that will happen upon the event being triggered
        //lets create first an object that will describe the contents of our alert box.
        // Swal.fire({
        //     title: 'Registered Successfully!',
        //     icon: 'success',
        //     text: 'Your New Account Has Been Created'
        // })
        console.log(firstName); 
        console.log(middleName);
        console.log(lastName);
        console.log(email);
        console.log(mobileNo); 
        console.log(password1); 
        console.log(gender);   

        fetch('https://stark-ravine-07314.herokuapp.com/users/register', {
            method: "POST", 
            headers: {
                'Content-Type': 'application/json' 
            },
            body: JSON.stringify({ 
                firstName: firstName,
                middleName: middleName,
                lastName: lastName,
                email: email,
                password: password1,
                gender: gender,
                mobileNo: mobileNo
            })
        }) 
        .then(res => res.json()) 
        .then(data => {
            
            Swal.fire({
                title:`Hey ${data.firstName}, your account is registered successfully!`,
                icon: 'success',
                text: 'Welcome to Zuitt!'
            })

            history.push('/login'); 

        }) 

    }   

    
    useEffect(() => {
        console.log(gender)
        
        if (password1 === password2 && password2 !== '') {
            setIsMatch(true);
        } else {
            setIsMatch(false);
        }

        
        if ((firstName !== '' && middleName !== '' &&  lastName !== '' &&  email !== '' && password1 !== '' && password2 !== '' &&  mobileNo !== '' && mobileNo.length === 11) &&  password2 === password2 )  {
            setRegisterBtnActive(true);
            setIsComplete(true);
           
        } else {
            setRegisterBtnActive(false);
            setIsComplete(false);
            
        }
    },[firstName, middleName, lastName, email, password1, password2, mobileNo]); 

    useEffect (()=>{

        const phStartNumber = ["0995", "0917", "0998", "0908", "0961", "0999", "0919", "0975" ]
        let phoneNumber = mobileNo.toString()
        let everyNumber = phStartNumber.find( element => element === phoneNumber.slice(0,4))
     
       if (mobileNo.slice(0,4) === everyNumber && mobileNo.length === 11){
        setIsValidNumber(true)
       }else {
        setIsValidNumber(false)
        }
      },[mobileNo]);
    
    return(
        <>
            <Container>
            {/* Banner Component/Greetings */}
                <Hero data={forBanner} />

                {/* <img className="mb-5" src="photo1.jpg" alt="img not found" /> */}


                {
                    isComplete ? 
                        <h1 className="mb-5"> Click The Button Below to Register Your Account! </h1> //Truthy
                    :
                        <h1 className="mb-5"> Fill up this Form Below: </h1> //Falsy
                }
                

                {/* Register Form Component */}
            <Form onSubmit={(event) => registerUser(event) } className="mb-5">
                {/* FirstName */}
                <Form.Group controlId="firstName">
                    <Form.Label>
                        First Name:
                    </Form.Label>
                    <Form.Control type="text" placeholder="Please Insert First Name Here" value={firstName} onChange={event => setFirstName(event.target.value)} required/>
                </Form.Group>

                {/* Middle Name */}
                <Form.Group controlId="middleName">
                    <Form.Label>
                        Middle Name:
                    </Form.Label>
                    <Form.Control type="text" placeholder="Please Insert Middle Name Here" value={middleName} onChange={event => setMiddleName(event.target.value)} required/>
                </Form.Group>

                {/* Last Name */}
                <Form.Group controlId="lastName">
                    <Form.Label>
                        Last Name:
                    </Form.Label>
                    <Form.Control type="text" placeholder="Please Insert Last Name Here" value={lastName} onChange={event => setLastName (event.target.value)} required/>
                </Form.Group>

                {/* Email */}
                <Form.Group controlId="userEmail">
                    <Form.Label>
                        Email Address:
                    </Form.Label>
                    <Form.Control type="email" placeholder="Please Insert email Here" value={email} onChange={event => setEmail (event.target.value)} required/>
                </Form.Group>

                {/* password */}
                <Form.Group controlId="password1">
                    <Form.Label>
                        Password:
                    </Form.Label>
                    <Form.Control type="password" placeholder="Please Insert Password Here" value={password1} onChange={event => setPassword1 (event.target.value)} required/>
                </Form.Group>
                
                {/* confirming/verify password */}
                <Form.Group controlId="password2">
                    <Form.Label>
                    Confirm Password:
                    </Form.Label>
                    
                    {
                        password1 === "" && password2 === "" ?
                    <strong className="text-success"></strong> :

                    isMatch ?
                        <strong className="text-success"> *Passwords Matched* </strong>
                    :
                        <strong className="text-danger"> *Passwords Should Match* </strong>
                    }

                    <Form.Control type="password" placeholder="Verify Password Here" value={password2} onChange={event => setPassword2 (event.target.value)} required/>
                </Form.Group>

                {/* gender */}
                <Form.Group controlId="gender">
                    <Form.Label>
                    Select Gender:
                    </Form.Label>
                    <Form.Control as="select" value={gender} onChange={(e) => setGender(e.currentTarget.value)}>
                        <option selected disabled>Select Gender Here</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                    </Form.Control>
                </Form.Group>

                {/* mobile number */}
                <Form.Group>
                    <Form.Label>
                    Mobile Number:
                    </Form.Label>
                    { 
                    isValidNumber ? 
                        <strong className="text-success"> *Mobile Number Registered* </strong>
                    :
                        <strong className="text-danger"> *Please input a valid number* </strong>
                    }
                    <Form.Control type="number" placeholder="Insert Mobile Number Here" onInput={(e) => (e.target.value = e.target.value.slice(0, 11))} value={mobileNo} onChange={event => setMobileNo (event.target.value)} required/>
                </Form.Group>

                {isRegisterBtnActive ? 
                <Button variant="success" className="btn btn-block" type="submit" id="submitBtn">
                    Create New Account
                </Button> 
                : 
                <Button variant="danger" className="btn btn-block" type="submit" id="submitBtn2" disabled>
                    Create New Account
                </Button>
                }

            </Form>
            </Container>
            <Footer foot="sticky-bottom"/>
        </>
    );
}